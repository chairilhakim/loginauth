<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Models\User;
use App\Models\Company;

class AjaxController extends Controller {

    public function __construct(){
        
    }
    
    public function getRemoveUser($id){
        $dataUser = Auth::user();
        $modelUser = New User;
        $modelCompany = New Company;
        $getData = $modelUser->getDetailUser($id);
        return view('admin.user.ajax-rm')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    

    
    

}
