<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Antrian;
use App\Models\Bpjs;

class ApiController extends Controller {
    
    public function __construct(){
        
    }
    
    public function postRegistration($no, $loket, Request $request){
        $modelAntrian = New Antrian;
        $getData = $modelAntrian->getDisplayFirstAntrian($loket);
        $getLastAntrian = $modelAntrian->getDisplayLastAntrianTuntas($loket);
        $no_antrian = 0;
        $desc = '-';
        if($getLastAntrian != null){
            $explode = explode('-', $getLastAntrian->code);
            $no_antrian = $explode[1];
            $desc = 'Loket '.$getLastAntrian->counter;
        }
        if($getData != null){
            $explode = explode('-', $getData->code);
            $no_antrian = $explode[1];
            $desc = 'Loket '.$getData->counter;
        }
        if($no == $no_antrian){
            return response()->json([
                'status' => 0,
                'no_antrian' => $no_antrian,
                'desc' => $desc,
            ]);
        }
        return response()->json([
            'status' => 1,
            'no_antrian' => $no_antrian,
            'desc' => $desc,
        ]);
    }
    
    public function postKiosk(Request $request){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);

        if($request->header("accept") != "application/json") {
            return response()->json([
                "status" => 0,
                "message" => "Format must application/json",
                "data" => null
            ]);
        }

        if($request->header("key") != "CITR224WJ9") {
            return response()->json([
                "status" => 0,
                "message" => "Key data invalid",
                "data" => null
            ]);
        }
        if($request->name == null || $request->phone == null || $request->payment == null || $request->id_card == null || $request->gender == null || $request->birth_place == null || $request->birthdayReg == null || $request->adrress == null || $request->city == null || $request->province == null || $request->department_id == null){
            return response()->json([
                "status" => 0,
                "message" => "data input tidak boleh kosong",
                "data" => null
            ]);
        }
        $modelBpjs = New Bpjs;
        $modelAntrian = New Antrian;
        $modelPatient = New Patient;
        $getPatient = $modelPatient->getCheckPatient($request->id_card);
        $getRegCode = $modelAntrian->getReservationCode();
        if($getPatient == null){
            $dataInsertPatient = array(
                'name' => $request->name,
                'phone' => $request->phone,
                'id_card' => $request->id_card,
                'id_bpjs' => $request->id_bpjs,
                'gender' => $request->gender,
                'birth_place' => $request->birth_place,
                'birth' => $request->birthdayReg,
                'address' => $request->adrress,
                'city' => $request->city,
                'province' => $request->province,
            );
            $insertPasien = $modelPatient->getInsertPatient('patient', $dataInsertPatient);
            $patient_id = $insertPasien->lastID;
        } else {
            $patient_id = $getPatient->id;
        }
        $visit = date('Y-m-d');
        $getDataAntrian = $modelAntrian->getAntrianDaily($request->payment, $visit);
        $dataInsert = array(
            'reg_code' => $getRegCode,
            'patient_id' => $patient_id,
            'department_id' => $request->department_id,
            'name' => $getPatient->name,
            'phone' => $getPatient->phone,
            'visit' => $visit,
            'payment' => $request->payment,
            'antrian_code' => $getDataAntrian
        );
        $modelAntrian->getInsertAntrian('registration_online', $dataInsert);
        
        $dataInsertAntrian = array(
            'ma_id' => $request->payment,
            'code' => $getDataAntrian,
            'antri_date' => $visit,
            'department_id' => $request->department_id,
        );
        $modelAntrian->getInsertAntrian('antrian', $dataInsertAntrian);
//        $getTimeStamp = $modelBpjs->tStamp();
        return response()->json([
            "status" => 1,
            "data" => $dataInsert
        ]);
    }
    
    public function postKioskOld(Request $request){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);

        if($request->header("accept") != "application/json") {
            return response()->json([
                "status" => 0,
                "message" => "Format must application/json",
                "data" => null
            ]);
        }

        if($request->header("key") != "CITR224WJ9") {
            return response()->json([
                "status" => 0,
                "message" => "Key data invalid",
                "data" => null
            ]);
        }
        if($request->rm_code == null || $request->payment == null){
            return response()->json([
                "status" => 0,
                "message" => "Data input tidak boleh kosong",
                "data" => null
            ]);
        }
        $modelBpjs = New Bpjs;
        $modelAntrian = New Antrian;
        $modelPatient = New Patient;
        $getPatient = $modelPatient->getCheckPatient($request->rm_code);
        if($getPatient == null){
            return response()->json([
                "status" => 0,
                "message" => "Data tidak ditemukan",
                "data" => null
            ]);
        }
        $visit = date('Y-m-d');
        $getRegCode = $modelAntrian->getReservationCode();
        $getDataAntrian = $modelAntrian->getAntrianDaily($request->payment, $visit);
        $dataInsert = array(
            'reg_code' => $getRegCode,
            'patient_id' => $getPatient->id,
            'department_id' => $request->department_id,
            'name' => $getPatient->name,
            'phone' => $getPatient->phone,
            'visit' => $visit,
            'payment' => $request->payment,
            'antrian_code' => $getDataAntrian
        );
        $modelAntrian->getInsertAntrian('registration_online', $dataInsert);
        
        $dataInsertAntrian = array(
            'ma_id' => $request->payment,
            'code' => $getDataAntrian,
            'antri_date' => $visit,
            'department_id' => $request->department_id,
        );
        $modelAntrian->getInsertAntrian('antrian', $dataInsertAntrian);
//        $getTimeStamp = $modelBpjs->tStamp();
        return response()->json([
            "status" => 1,
            "data" => $dataInsert
        ]);
    }
    
    public function postAllRegistration(Request $request){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);

        if($request->header("accept") != "application/json") {
            return response()->json([
                "status" => 0,
                "message" => "Format must application/json",
                "data" => null
            ]);
        }

        if($request->header("key") != "CITR224WJ9") {
            return response()->json([
                "status" => 0,
                "message" => "Key data invalid",
                "data" => null
            ]);
        }
        
        $modelPatient = New Patient;
        $getData = $modelPatient->getAllPatient();
        return response()->json([
            "status" => 1,
            "message" => '',
            "data" => $getData
        ]);
    }
    
    public function postGetAllPatientFromSIMRS(Request $request){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);

        if($request->header("accept") != "application/json") {
            return response()->json([
                "status" => 0,
                "message" => "Format must application/json",
                "data" => null
            ]);
        }

        if($request->header("key") != "CITR224WJ9") {
            return response()->json([
                "status" => 0,
                "message" => "Key data invalid",
                "data" => null
            ]);
        }
        
        $modelPatient = New Patient;
        $modelBPJS = New Bpjs;
        
         //get All patient from simrs
        $url = 'https://localhost-endpoint-simrs:8080/get-patient';
        $headers = array(
            'CMX-UID:UID',
            'CMX-Token:TokenSession'
        );
        $body = array(
            'body' => array()
        );
        $getData = $modelBPJS->curlPostAPI($url, $headers, $body);
        $getAlldata = json_decode($getData, true);
        foreach($getAlldata as $row){
            //Cek Ada ga
            $getCek = $modelPatient->getCheckPatient($row->ktp);
            if($getCek == null){
                $dataInsert = array(
                    'name' => $row->name,
                    'phone' => $row->phone,
                    'rm_code' => $row->rm,
                    'id_card' => $row->ktp,
                    'id_bpjs' => $row->bpjs,
                    'gender' => $row->gender,
                    'birth_place' => $row->birth_place,
                    'birth' => $row->birth,
                    'address' => $row->adrress,
                    'city' => $row->city,
                    'province' => $row->province,
                    'status' => 1,
                    'status_at' => $row->created_at
                );
//              $modelPatient->getInsertPatient('patient', $dataInsert);
            }
            if($getCek != null){
                $updatePatient = array(
                    'rm_code' => $row->rm,
                    'id_card' => $row->ktp,
                    'id_bpjs' => $row->bpjs,
                );
//                $modelPatient->getUpdatePatient('patient', 'id', $getCek->id, $updatePatient);
            }
        }
        
        $dataReturn = array(
            'status' => 1,
            'draw' => $getAlldata
        );
        return response()->json($dataReturn);
    }
    
    public function postJKN(Request $request){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);

        if($request->header("accept") != "application/json") {
            return response()->json([
                "status" => 0,
                "message" => "Format must application/json",
                "data" => null
            ]);
        }

        if($request->header("key") != "CITR224WJ9") {
            return response()->json([
                "status" => 0,
                "message" => "Key data invalid",
                "data" => null
            ]);
        }
        if($request->name == null || $request->phone == null || $request->payment == null || $request->id_card == null || $request->gender == null || $request->birth_place == null || $request->birthdayReg == null || $request->adrress == null || $request->city == null || $request->province == null || $request->department_id == null){
            return response()->json([
                "status" => 0,
                "message" => "data input tidak boleh kosong",
                "data" => null
            ]);
        }
        $modelBpjs = New Bpjs;
        $modelAntrian = New Antrian;
        $modelPatient = New Patient;
        $getPatient = $modelPatient->getCheckPatient($request->id_card);
        $getRegCode = $modelAntrian->getReservationCode();
        if($getPatient == null){
            $dataInsertPatient = array(
                'name' => $request->name,
                'phone' => $request->phone,
                'id_card' => $request->id_card,
                'id_bpjs' => $request->id_bpjs,
                'gender' => $request->gender,
                'birth_place' => $request->birth_place,
                'birth' => $request->birthdayReg,
                'address' => $request->adrress,
                'city' => $request->city,
                'province' => $request->province,
            );
            $insertPasien = $modelPatient->getInsertPatient('patient', $dataInsertPatient);
            $patient_id = $insertPasien->lastID;
        } else {
            $patient_id = $getPatient->id;
        }
        $visit = date('Y-m-d');
        $getDataAntrian = $modelAntrian->getAntrianDaily($request->payment, $visit);
        $dataInsert = array(
            'reg_code' => $getRegCode,
            'patient_id' => $patient_id,
            'department_id' => $request->department_id,
            'name' => $getPatient->name,
            'phone' => $getPatient->phone,
            'visit' => $visit,
            'payment' => $request->payment,
            'antrian_code' => $getDataAntrian,
            'type' => 3
        );
        $modelAntrian->getInsertAntrian('registration_online', $dataInsert);
        
        $dataInsertAntrian = array(
            'ma_id' => $request->payment,
            'code' => $getDataAntrian,
            'antri_date' => $visit,
            'department_id' => $request->department_id,
        );
        $modelAntrian->getInsertAntrian('antrian', $dataInsertAntrian);
        $getTimeStamp = $modelBpjs->tStamp();
        return response()->json([
            "status" => 1,
            "data" => $dataInsert
        ]);
    }
    
    public function postJKNOld(Request $request){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);

        if($request->header("accept") != "application/json") {
            return response()->json([
                "status" => 0,
                "message" => "Format must application/json",
                "data" => null
            ]);
        }

        if($request->header("key") != "CITR224WJ9") {
            return response()->json([
                "status" => 0,
                "message" => "Key data invalid",
                "data" => null
            ]);
        }
        if($request->rm_code == null || $request->payment == null){
            return response()->json([
                "status" => 0,
                "message" => "Data input tidak boleh kosong",
                "data" => null
            ]);
        }
        $modelBpjs = New Bpjs;
        $modelAntrian = New Antrian;
        $modelPatient = New Patient;
        $getPatient = $modelPatient->getCheckPatient($request->rm_code);
        if($getPatient == null){
            return response()->json([
                "status" => 0,
                "message" => "Data tidak ditemukan",
                "data" => null
            ]);
        }
        $visit = date('Y-m-d');
        $getRegCode = $modelAntrian->getReservationCode();
        $getDataAntrian = $modelAntrian->getAntrianDaily($request->payment, $visit);
        $dataInsert = array(
            'reg_code' => $getRegCode,
            'patient_id' => $getPatient->id,
            'department_id' => $request->department_id,
            'name' => $getPatient->name,
            'phone' => $getPatient->phone,
            'visit' => $visit,
            'payment' => $request->payment,
            'antrian_code' => $getDataAntrian,
            'type' => 3
        );
        $modelAntrian->getInsertAntrian('registration_online', $dataInsert);
        
        $dataInsertAntrian = array(
            'ma_id' => $request->payment,
            'code' => $getDataAntrian,
            'antri_date' => $visit,
            'department_id' => $request->department_id,
        );
        $modelAntrian->getInsertAntrian('antrian', $dataInsertAntrian);
        $getTimeStamp = $modelBpjs->tStamp();
        return response()->json([
            "status" => 1,
            "data" => $dataInsert
        ]);
    }
    
    
}