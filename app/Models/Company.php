<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Company extends Model {
    
    public function getInsertCompany($data){
        try {
            $lastInsertedID = DB::table('company')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateCompany($fieldName, $name, $data){
        try {
            DB::table('company')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllCompany(){
        $sql = DB::table('company')
                    ->orderBy('id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getCompanyValidate($name){
        $sql = DB::table('company')
                    ->selectRaw('id')
                    ->where('name', '=', $name)
                    ->first();
        return $sql;
    }
    
    public function getDetailCompany($id){
        $sql = DB::table('company')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getNameValidateEdit($name, $id){
        $sql = DB::table('company')
                    ->selectRaw('id')
                    ->where('name', '=', $name)
                    ->where('id', '!=', $id)
                    ->first();
        return $sql;
    }
    
   
    
    
}