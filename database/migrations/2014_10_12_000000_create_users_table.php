<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('f_name', 50);
            $table->string('l_name', 50);
            $table->string('phone', 20)->nullable();
            $table->string('email', 40);
            $table->string('password', 100);
            $table->tinyInteger('is_active')->default(1)->comment('0 = inactive, 1 = active');
            $table->smallInteger('user_type')->default(10)->comment('1 = admin, 10 = user');
            $table->integer('company_id')->nullable();
            
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->rememberToken();
            
            $table->index('f_name');
            $table->index('l_name');
            $table->index('phone');
            $table->index('password');
            $table->index('email');
            $table->index('is_active');
            $table->index('user_type');
            $table->index('company_id');
            
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down() {
        Schema::dropIfExists('users');
    }
}
