<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder {
    
    public function run() {
        $company = array(
            array(
                'name' => 'GRTech', 
                'email' => 'company@grtech.com.my',
                'logo' => 'company.png',
                'website' => 'https://grtech.com/'
            ),
        );
        foreach($company as $rowCompany){
            DB::table('company')->insert($rowCompany);
        }
        
        $users = array(
            array(
                'f_name' => 'Admin',
                'l_name' => 'Company',
                'phone' => '987678',
                'password' => bcrypt('password'),
                'email' => 'admin@grtech.com.my',
                'is_active' => 1,
                'user_type' => 1,
            ),
            array(
                'f_name' => 'User',
                'l_name' => 'Company',
                'phone' => '678654',
                'password' => bcrypt('password'),
                'email' => 'user@grtech.com.my',
                'is_active' => 1,
                'user_type' => 10,
                'company_id' => 1
            )
        );
        foreach($users as $row){
            DB::table('users')->insert($row);
        }
        
        dd('done seed');
    }
    
}
