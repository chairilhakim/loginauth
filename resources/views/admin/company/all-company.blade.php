@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>
                            Companies
                            <a class="badge badge-primary" href="{{ URL::to('/') }}/add/company">Add</a>
                        </h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered table-xs" id="simpletable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Logo</th>
                                        <th>Website</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getAllCompany != null)
                                        @foreach($getAllCompany as $row)
                                            
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>
                                                    @if($row->logo != null)
                                                    <img src="/image/{{$row->logo}}" style="width: 70px;">
                                                    @endif
                                                </td>
                                                <td><a href="{{$row->website}}" target="_blank">{{$row->website}}</a></td>
                                                <td>
                                                    <a class="badge badge-info" href="/edit/company/{{$row->id}}">edit</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section("styles")
<link rel="stylesheet" href="/assets/css/dataTables.bootstrap4.min.css"/>
@endsection

@section("javascript")
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
            $("#simpletable").DataTable({
                "paging": true,
                "lengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "all"]],
                "searching": true,
                "ordering": false,
                "info": false,
                "responsive": true,
            });
        });
    </script>
@endsection
