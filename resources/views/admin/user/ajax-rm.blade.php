<form class="login100-form validate-form" method="post" action="/rm/user">
    <div class="modal-header">
        <h5 class="modal-title text-danger" id="exampleModalLabel">Do you want to remove this user?</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" class="form-control" readonly="" value="{{$getData->f_name}} {{$getData->l_name}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" readonly=""  class="form-control" value="{{$getData->email}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Type</label>
                    <input type="text" class="form-control" readonly="" value="{{$getData->name_company}}">
                </div>
            </div>
            <input type="hidden" name="getid" value="{{$getData->id}}" >
        </div>
            
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        <div class="divider"></div>
        @if($getData != null)
        <div class="right-side">
            <button type="submit" class="btn btn-info">Submit</button>
        </div>
        @endif
    </div>
</form>   