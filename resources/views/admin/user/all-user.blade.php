@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>
                            Users
                            <a class="badge badge-primary" href="{{ URL::to('/') }}/add/user">Add</a>
                        </h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered table-xs" id="simpletable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Company</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getAllUser != null)
                                        @foreach($getAllUser as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{$row->f_name}} {{$row->l_name}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->phone}}</td>
                                                <td>{{$row->name_company}}</td>
                                                <td>
                                                    <a class="badge badge-info" href="/edit/user/{{$row->id}}">edit</a>
                                                    <a rel="tooltip"  data-toggle="modal" data-target="#popUp"  href="{{ URL::to('/') }}/ajax/rm/user/{{$row->id}}" class="badge badge-danger" >remove</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section("styles")
<link rel="stylesheet" href="/assets/css/dataTables.bootstrap4.min.css"/>
@endsection

@section("javascript")
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
            $("#simpletable").DataTable({
                "paging": true,
                "lengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "all"]],
                "searching": true,
                "ordering": false,
                "info": false,
                "responsive": true,
            });
        });
    </script>
    <script type="text/javascript">
    $("#popUp").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

</script>
@endsection
