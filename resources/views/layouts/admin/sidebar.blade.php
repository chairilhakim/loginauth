<nav class="pcoded-navbar menupos-fixed menu-light ">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div " >
            <ul class="nav pcoded-inner-navbar ">
                <li class="nav-item pcoded-menu-caption">
                    <label>Menu</label>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::to('/') }}/dashboard" class="nav-link ">
                        <span class="pcoded-micon">
                            <i class="feather icon-home"></i>
                        </span>
                        <span class="pcoded-mtext">
                            Dashboard
                        </span>
                    </a>
                </li>
                
                @if($dataUser->user_type == 1)
                <li class="nav-item">
                    <a href="/company" class="nav-link ">
                        <span class="pcoded-micon">
                            <i class="feather icon-globe"></i>
                        </span>
                        <span class="pcoded-mtext">
                            Company
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/users" class="nav-link ">
                        <span class="pcoded-micon">
                            <i class="feather icon-users"></i>
                        </span>
                        <span class="pcoded-mtext">
                            User
                        </span>
                    </a>
                </li>
                @endif
                
               
                
                
                <li class="nav-item">
                    <a href="{{ URL::to('/') }}/logout" class="nav-link "><span class="pcoded-micon"><i class="feather icon-log-out text-c-red"></i></span><span class="pcoded-mtext">Logout</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>