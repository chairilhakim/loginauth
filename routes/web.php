<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\Admin\HomeController@getFront')->name('frontLogin');

Route::get('/login_admin', 'App\Http\Controllers\Admin\HomeController@getMemberLogin')->name('memberLogin');
Route::post('/login_admin', 'App\Http\Controllers\Admin\HomeController@postMemberLogin');
Route::get('image/{filename}', 'App\Http\Controllers\Admin\HomeController@displayImage')->name('image.displayImage');

Route::prefix('/')->group(function () {
    Route::get('/logout', 'App\Http\Controllers\Admin\HomeController@getAdminLogout')->middleware('auth');
    Route::get('/dashboard', 'App\Http\Controllers\Admin\HomeController@getDashboard')->name('admDashboard')->middleware('auth');
    
    Route::get('/users', 'App\Http\Controllers\Admin\MasteradminController@getAllUser')->name('listUsers')->middleware('auth');
    Route::get('/add/user', 'App\Http\Controllers\Admin\MasteradminController@getAddUser')->name('addUser')->middleware('auth');
    Route::post('/add/user', 'App\Http\Controllers\Admin\MasteradminController@postAddUser')->middleware('auth');
    Route::get('/edit/user/{id}', 'App\Http\Controllers\Admin\MasteradminController@getEditUser')->name('EditUser')->middleware('auth');
    Route::post('/edit/user', 'App\Http\Controllers\Admin\MasteradminController@postEditUser')->middleware('auth');
    Route::post('/rm/user', 'App\Http\Controllers\Admin\MasteradminController@postRemoveUser')->middleware('auth');
    
    
    Route::get('/company', 'App\Http\Controllers\Admin\MasteradminController@getAllCompany')->name('listCompany')->middleware('auth');
    Route::get('/add/company', 'App\Http\Controllers\Admin\MasteradminController@getAddCompany')->name('addCompany')->middleware('auth');
    Route::post('/add/company', 'App\Http\Controllers\Admin\MasteradminController@postAddCompany')->middleware('auth');
    Route::get('/edit/company/{id}', 'App\Http\Controllers\Admin\MasteradminController@getEditCompany')->name('EditCompany')->middleware('auth');
    Route::post('/edit/company', 'App\Http\Controllers\Admin\MasteradminController@postEditCompany')->middleware('auth');
    Route::post('/rm/company', 'App\Http\Controllers\Admin\MasteradminController@postRemoveCompany')->middleware('auth');
    
    //Ajax
    Route::get('/ajax/rm/user/{id}', 'App\Http\Controllers\Admin\AjaxController@getRemoveUser')->middleware('auth');
    Route::get('/ajax/rm/company/{id}', 'App\Http\Controllers\Admin\AjaxController@getRemoveCompany')->middleware('auth');
    
});
